#!/bin/python

import argparse
import numpy as np
import matplotlib.pyplot as plt

#Unbinned GoF test: follows ref arXiv:1006.3019v2

"""
Unbinned g.o.f test: Mixed Sample
"""
def I_sumk_p_MixedSample(p, data, mc, p_isdata, weight, nk): 
    """
    Calculate test statistic T for mixed sample test:
        p       : A point from data or mc sample with array shape (n_dim,)
        data    : Data sample with shape (ndata,n_dim) 
        mc      : MC sampel with shape (nmcs, n_dim) 
        p_isdata: Whether p belongs to data or mc sample.
        weight  : Weight for the normalised Euclidean distance.
        n_k     : Number of neighbours to choose
    """
    #Evaluate the nk nearest neighbors points for a given point p
    nearest_data_pts = None; nearest_mc_pts  = None
    #Calculate the normalised Euclidean distance for the given point
    delta_data = (p - data) #(ndim,) - (ndata, ndim) = (ndata, ndim)
    delta_mc   = (p - mc)   #(ndim,) - (nmc  , ndim) = (nmc  , ndim)
    norm_data  = (delta_data/weight)**2 #(ndata, ndim)/(ndim,) = (ndata, ndim)
    norm_mc    = (delta_mc/weight)**2   #(nmc  , ndim)/(ndim,) = (nmc  , ndim)
    norm_data_s= np.sum(norm_data, axis=1)#(ndata,)
    norm_mc_s  = np.sum(norm_mc  , axis=1)#(nmc,)
    #Sort to get the ascending distance from point p
    sorted_data= np.sort(norm_data_s) #(ndata,)
    sorted_mc  = np.sort(norm_mc_s)   #(nmc,)
    #First remove the point p from corresponding sample (this is not very optimal way of doing it instead remove the point p from the corresponding sample)
    if p_isdata: 
        nearest_data_pts = np.sort(sorted_data)[1:nk+1] #shape (ndata,) but choose nk
        nearest_mc_pts   = np.sort(sorted_mc)[0:nk]     #shape (nmc,) but choose nk
    else:
        nearest_data_pts = np.sort(sorted_data)[0:nk] 
        nearest_mc_pts   = np.sort(sorted_mc)[1:nk+1] 

    #combine the neighbours
    neighbors = np.sort(np.concatenate([nearest_data_pts,nearest_mc_pts]))[:nk] 

    #For a point, given that it belongs to a class, calculate the number of nearby by points of same class
    i_sumk_p = 0.
    for k in neighbors: #this is should work even in case of same dist b/w two classes 
        if k in nearest_data_pts and p_isdata: 
            i_sumk_p += 1.
        elif k in nearest_mc_pts and not p_isdata: 
            i_sumk_p += 1.

    return i_sumk_p

def get_pvalue_MixedSample(data, mc, n_k = 10, weight_type = 'RMS', plot = True):
    """
    Get p-value using mixed sample test:
        data       : Data sample with shape (ndata,n_dim) 
        mc         : MC sampel with shape (nmcs, n_dim) 
        n_k        : (optional) Nearest Neighbors to a given point. There is limit on n_mc and n_k that is imposed in this method i.e. n_mc = 10*ndata, nk=10 was optimal (factor 10 makes it worse)
        weight_type: (optional) weight for the Euclidean distance. Options available are RMS, MaxMin, Ones
        plot       : (optional) Set to true if you want plotting of test statistics
    """
    #get the sizes of the mc, data and total samples
    n_mc = len(mc); n_data = len(data); n_tot = n_mc + n_data
    #print('n_mc, n_data, n_tot', n_mc, n_data, n_tot)

    #Choose a user defined weight
    sample = np.concatenate((data,mc), axis=0) #combine sample, shape = (nmc+ndata, n_dim)
    #print('sample with shape = (nmc+ndata, n_dim)', sample, sample.shape)
    weight = None #shape = (n_dim,)
    if weight_type == 'RMS':
        weight = np.sqrt(1./len(sample) * np.sum(sample**2., axis=0))
    elif weight_type == 'MaxMin':
        weight = np.max(sample, axis=0) - np.min(sample, axis=0)
    elif weight_type == 'Ones':
        weight = np.ones(sample.shape[1])
    else:
        raise Exception("weight_type available are only RMS or MaxMin or Uniform")

    #print('weight with shape = (n_dim,)', weight, weight.shape)

    #calculate test statistic and GoF
    mu_T    = (n_mc*(n_mc -1.) + n_data*(n_data-1.))/(n_tot*(n_tot-1.))
    sigma_T = np.sqrt(1./(n_tot*n_k) * (n_mc*n_data/n_tot**2 + (2.*n_mc*n_data/n_tot**2)**2))
    #print('mu_T, sigma_T', mu_T, sigma_T)
    T       =  1./(n_k*n_tot) * sum([I_sumk_p_MixedSample(p,data,mc,p_isdata=False,weight=weight,nk=n_k) for p in mc]) 
    T      +=  1./(n_k*n_tot) * sum([I_sumk_p_MixedSample(p,data,mc,p_isdata=True ,weight=weight,nk=n_k)  for p in data])
    T_observed  = (T - mu_T)/sigma_T

    #get pvalue with a std gaussian dist.
    std_norm = np.random.normal(size=500000) #Since the test statistic follows a normal distribution
    xmin = np.min(std_norm)
    xmax = np.max(std_norm)
    condbelow= std_norm<T_observed
    condup   = std_norm>T_observed
    p_val = len(std_norm[std_norm>T_observed])/float(len(std_norm))

    if plot:
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        ax.hist(std_norm[condbelow], range=(xmin, xmax), bins = 80, color='blue', label = 'Test statistic (Null hypothesis)')
        ax.hist(std_norm[condup], range=(xmin, xmax), bins = 80, color='orange' )
        ax.axvline(T_observed, color='r', linestyle = '--', label = 'Observed test statistic')
        ax.set_yscale('log')
        ax.legend(loc='best')
        fig.tight_layout()
        fig.savefig('MixedSample'+case+'.pdf', bbox_inches =  'tight')

    print('Test statistic observed:', T_observed)
    return p_val

def main():
    #generate multiple data sets with a parent pdf = 2D Gaussian
    np.random.seed(seed)
    mean = [1.,1.5]
    cov  = [[1.4, 0.5], [0.5, 1]]
    data = np.random.multivariate_normal(mean,cov,ndata)
    
    #generate a test sample with the hypothesis either that (test pdf == parent pdf) or (test pdf != parent pdf)
    np.random.seed(1000)
    if case == 'Case2' or case == 'Case3':
        print('Test PDF not equal to parent pdf with which data was generated')
        if case == 'Case2':
            mean = [1.,1.5]
            cov  = [[1.4,-0.05], [-0.05, 1]]
        elif case == 'Case3':
            mean = [4.,7.5]
            cov  = [[1.4, 0.1], [0.1, 1.4]]
        else:
            raise Exception('Case not recognised!')

    mc   = np.random.multivariate_normal(mean,cov,nMC)

    #plot data and mc
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(mc[:, 0], mc[:,1], color = 'r', label = 'Fit', alpha = 0.4)
    ax.scatter(data[:, 0], data[:,1], color = 'b', label = 'Data', alpha = 0.4)
    ax.legend(loc='best')
    fig.tight_layout()
    fig.savefig('DataMC'+case+'.pdf', bbox_inches = 'tight')

    #NB: To calculate p-value it generates a std normal distribution (since test statistic distribution is known apriori). Does not extrapolate the tail, but just
    #calculates the fraction of toys above observed test statistic
    p_val = get_pvalue_MixedSample(data, mc, n_k = nearest_neighbours, weight_type = eucl_weight_type, plot = True)
    print('P-value:', p_val)

if __name__ == "__main__":
    parser     = argparse.ArgumentParser(description='Arguments for MixedSample.py')
    parser.add_argument('-s'  , '--seed'              , dest='seed'              ,type=int     ,default=1       ,help='(int) Seed for generation of fake/toy data. Default is 1.')
    parser.add_argument('-nd' , '--ndata'             , dest='ndata'             ,type=int     ,default=1000    ,help='(int) Sample size for fake/toy data to generate. Test sample will be 10 times this. Default is 1000.')
    parser.add_argument('-nmc', '--nMC'               , dest='nMC'               ,type=int     ,default=10000   ,help='(int) Sample size for test sample. Default is 10 times that of default ndata.')
    parser.add_argument('-nk' , '--nearest_neighbours', dest='nearest_neighbours',type=int     ,default=10      ,help='(int) Number of nearest neighbors to a given point to consider. Default is 10.')
    parser.add_argument('-ewt', '--eucl_weight_type'  , dest='eucl_weight_type'  ,type=str     ,default='MaxMin',help='(str) Type of weight used for euclidean distance. Options are [MaxMin, RMS, Ones]. Default is MaxMin.')
    parser.add_argument('-c'  , '--case'              , dest='case'              ,type=str     ,default='Case1' ,help='(str) Case study to run. Options are Case[1,2,3]. \
                                                                                                                      Case1: Data and Fit samples are from same PDF. \
                                                                                                                      Case2: Data and Fit samples are from different PDF. However PDFs are only slightly different.\
                                                                                                                      Case3: Data and Fit samples are from different PDF. However PDFs are very different. \
                                                                                                                      Default is Case1')

    args       = parser.parse_args()
    seed       = args.seed
    ndata      = args.ndata
    nMC        = args.nMC
    nearest_neighbours  = args.nearest_neighbours
    eucl_weight_type    = args.eucl_weight_type
    case  = args.case
    print(args)

    main()
