#!/bin/python

import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal

#Unbinned GoF test: follows ref arXiv:1006.3019v2

"""
Unbinned g.o.f test: Energy test
"""
def phi_EnergyTest(sample_i, w, sigma_i, kernel_type, sample_j = None, sigma_j = None): 
    """
    Evaluate Phi for energy test:
        sample_i : Either data or mc with shape (ndata,n_dim) or (nmc,n_dim) 
        w        : Weight to calculate the normalised euclidean distance.
        sigma_i  : sigma for the sample_i in phi function
        kernel_type: Name of weighting function to be used
        sample_j : (Optional) Used when calculating correlation b/w data and mc. To calculate this correlation if 'sample_i' is data then 'sample_j' should be mc.
        sigma_j  : (Optional) sigma for the sample_j in phi function
    """
    #define the kernel/weighting function
    kernelfunc = None
    if kernel_type == 'Exponential':
        kernelfunc = lambda dist,sigmasq: np.exp(-0.5 * dist/sigmasq)

    phi_sum = None
    if type(sample_j) == np.ndarray: #NB: np.sum axis=1 gives shape (n_sample1,)
        if type(sigma_j) == np.ndarray: #NB: np.sum axis=1 gives shape (n_sample1,)
            phi_sum = 0.
            for i, p_i in enumerate(sample_i):
                sgm_i    = sigma_i[i]
                sgm_j    = sigma_j
                delta_x  = (p_i - sample_j)  #(ndim,) - (nmc, ndim) = (nmc, ndim)
                norm_x   = (delta_x/w)**2  #(nmc, ndim)/(ndim,)   = (nmc, ndim)
                norm_x_s = np.sum(norm_x, axis=1) #(nmc,)
                sigmasq  = sgm_i * sgm_j #(nmc,)
                exp_slpe = kernelfunc(norm_x_s, sigmasq) #(nmc,)/(nmc,) = (nmc,)
                phi_sum += np.sum(exp_slpe)
    else: #NB: For this phi we have in the sum i<j hence the below gymnastics- the np.sum axis=1 gives shape (npoints-i+1,)
        phi_sum = 0.
        for i, p_i in enumerate(sample_i):
            sgm_i = sigma_i[i]
            if i+1 != len(sample_i):
                p_j      = sample_i[i+1:] #(nmc, ndim)
                sgm_j  = sigma_i[i+1:]
                delta_x  = (p_i - p_j)  #(ndim,) - (nmc, ndim) = (nmc, ndim)
                norm_x   = (delta_x/w)**2  #(nmc, ndim)/(ndim,)   = (nmc, ndim)
                norm_x_s = np.sum(norm_x, axis=1) #(nmc,)
                sigmasq  = sgm_i * sgm_j #(nmc,)
                exp_slpe = kernelfunc(norm_x_s, sigmasq) #(nmc,)/(nmc,) = (nmc,)
                phi_sum += np.sum(exp_slpe)

    return phi_sum

def get_Tstat_EnergyTest(data, mc, kernel_type, weight_type, sigma_list, include_mcterm = False):
    """
    Evaluate the test statistic for energy test:
        data       : Data sample with shape (ndata,n_dim) 
        mc         : MC sampel with shape (nmcs, n_dim) 
        kernel_type: Name of weighting function to be used
        weight_type: weight for the Euclidean distance. Options available are RMS, MaxMin, Ones
        sigma_list : [sigma_data, sigma_mc] for the phi function
        include_mcterm: (optional) If True includes MC term in the phi function calculation or else ignored
    """

    #Combine data and mc
    sample = np.concatenate((data,mc), axis=0)

    #get weight for normalised euclidean distance for data, mc and combined sample
    if weight_type == 'MaxMin':
        #w_data = np.max(data, axis=0) - np.min(data, axis=0)
        #w_mc   = np.max(mc  , axis=0) - np.min(mc  , axis=0)
        w_comb = np.max(sample, axis=0) - np.min(sample, axis=0)
    elif weight_type == 'RMS':
        #w_data = np.sqrt(1./len(data) * np.sum(data**2.,axis=0))
        #w_mc   = np.sqrt(1./len(mc) * np.sum(mc**2.,axis=0))      
        w_comb = np.sqrt(1./len(sample) * np.sum(sample**2.,axis=0))    
    elif weight_type == 'Ones':
        #w_data = np.ones(data.shape[1])
        #w_mc   = np.ones(mc.shape[1])
        w_comb = np.ones(sample.shape[1])
    else:
        raise Exception("weight_type available are only RMS or MaxMin or Uniform")

    #make phi funcs
    sigma_data   = sigma_list[0]
    sigma_mc     = sigma_list[1]
    sum_phi_data = phi_EnergyTest(data, w_comb, sigma_data, kernel_type)
    sum_phi_comb = phi_EnergyTest(data, w_comb, sigma_data, kernel_type, sample_j = mc, sigma_j = sigma_mc)
    if include_mcterm: 
        sum_phi_mc   = phi_EnergyTest(mc  , w_comb  , sigma_mc, kernel_type)
    else:
        sum_phi_mc   = 0.

    #make test statistic
    nd = len(data); nmc = len(mc)
    T  = 1./(nd*(nd-1.)) * sum_phi_data + 1./(nmc*(nmc-1.)) * sum_phi_mc - 1./(nmc*nd) * sum_phi_comb
    return T

def get_pvalue_EnergyTest(data, mc, kernel_type, weight_type = 'MaxMin', sigma_bar = 0.01, sigma_type = 'Constant', pdffunc = None, npermuations = 100, plot = True, include_mcterm = False):
    """
    Get p-value using test:
        data        : Data sample with shape (ndata,n_dim) 
        mc          : MC sampel with shape (nmcs, n_dim) 
        kernel_type: Name of weighting function to be used
        weight_type : (optional) weight for the Euclidean distance. Options available are RMS, MaxMin, Ones
        sigma_bar   : (optional) sigma_bar for the phi function
        sigma_type  : (optional) What type of sigma. Constant or otherwise?
        pdffunc     : (optional) The function for the test pdf only if 'sigma_type != Constant'.
        npermuations: (optional) permutations to run for the p-value.
        include_mcterm: (optional) If True includes MC term in the phi function calculation or else ignored
    """

    #function to calculate sigma for phi function
    def _calculate_sigmas(data, mc):
        #make combined smpl
        smpl     = np.concatenate((data,mc), axis=0)

        if sigma_type == 'Constant':
            sigma_data   = sigma_bar * np.ones_like(data[:,0])
            sigma_mc     = sigma_bar * np.ones_like(mc[:,0])
        else:
            if pdffunc is None:
                raise('Asked for sigma to be non-constant but pdffunc is None! Check and pass callable pdffunc')
            #calculate volume
            deltaxy = np.max(smpl, axis=0) - np.min(smpl, axis=0) 
            vol     = np.multiply.reduce(deltaxy)
            #calculate non-constant sigmas
            sigma_data   = sigma_bar * 1./(vol * pdffunc(data))
            sigma_mc     = sigma_bar * 1./(vol * pdffunc(mc))
            #make list of sigma for each smpl

        sigma_list = [sigma_data, sigma_mc]
        return sigma_list

    #get sigma for phi function
    sigma_lst = _calculate_sigmas(data , mc)

    #Test statistic of observed data
    T_observed = get_Tstat_EnergyTest(data, mc, kernel_type, weight_type, sigma_lst, include_mcterm = include_mcterm)
    print('Test statistic observed:', T_observed)

    #Test statstic distribution with permutation test
    T_values = np.zeros(npermuations)
    sample   = np.concatenate((data,mc), axis=0)
    for i in range(len(T_values)):
        newsample = np.random.permutation(sample)
        newdata   = newsample[:len(data)]
        newmc     = newsample[len(data):]
        newsigma_lst = _calculate_sigmas(newdata , newmc)
        T_values[i]  = get_Tstat_EnergyTest(newdata, newmc, kernel_type, weight_type, newsigma_lst, include_mcterm = include_mcterm)
        if i%10 == 0: print('At loop level', i)

    xmin     = np.min(T_values)
    xmax     = np.max(T_values)
    condbelow= T_values<T_observed
    condup   = T_values>T_observed
    #calculate p_value
    p_val = len(T_values[T_values>T_observed])/float(len(T_values))

    if plot:
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        ax.hist(T_values[condbelow], range=(xmin, xmax), bins = 40, color='blue', label = 'Test statistic (Null hypothesis)')
        ax.hist(T_values[condup], range=(xmin, xmax), bins = 40, color='orange' )
        ax.axvline(T_observed, color='r', linestyle = '--', label = 'Observed test statistic')
        ax.set_yscale('log')
        ax.legend(loc='best')
        fig.tight_layout()
        fig.savefig('EnergyTest'+case+'.pdf', bbox_inches =  'tight')

    return p_val

def str2bool(v):
    """
    Function used for argparse to convert string to boolean
    """
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    #generate multiple data sets with a parent pdf = 2D Gaussian
    np.random.seed(seed)
    mean = [1.,1.5]
    cov  = [[1.4, 0.5], [0.5, 1]]
    data = np.random.multivariate_normal(mean,cov,ndata)
    
    #generate a test sample with the hypothesis either that (test pdf == parent pdf) or (test pdf != parent pdf)
    np.random.seed(1000)
    if case == 'Case2' or case == 'Case3':
        print('Test PDF not equal to parent pdf with which data was generated')
        if case == 'Case2':
            mean = [1.,1.5]
            cov  = [[1.4,-0.05], [-0.05, 1]]
        elif case == 'Case3':
            mean = [4.,7.5]
            cov  = [[1.4, 0.1], [0.1, 1.4]]
        else:
            raise Exception('Case not recognised!')

    mc   = np.random.multivariate_normal(mean,cov,10*ndata)

    pdffunc = None
    if sigma_type == 'PDFDependent':
        #make a callable function
        pdffunc = lambda x: multivariate_normal.pdf(x, mean=mean, cov=cov);

    #plot data and mc
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(mc[:, 0], mc[:,1], color = 'r', label = 'Fit', alpha = 0.4)
    ax.scatter(data[:, 0], data[:,1], color = 'b', label = 'Data', alpha = 0.4)
    ax.legend(loc='best')
    fig.tight_layout()
    fig.savefig('DataMC'+case+'.pdf', bbox_inches = 'tight')
    
    #calculate p-value
    p_val = get_pvalue_EnergyTest(data, mc, kernel_type, weight_type = eucl_weight_type, sigma_bar = sigma_bar, sigma_type = sigma_type, pdffunc = pdffunc, npermuations = npermuations, include_mcterm = include_mcterm)
    print('P-value:', p_val)

if __name__ == "__main__":
    parser     = argparse.ArgumentParser(description='Arguments for EnergyTest.py')
    parser.add_argument('-s'  , '--seed'              , dest='seed'              ,type=int     ,default=1         ,help='(int) Seed for generation of fake/toy data. Default is 1.')
    parser.add_argument('-n'  , '--ndata'             , dest='ndata'             ,type=int     ,default=1000      ,help='(int) Sample size for fake/toy data to generate. Test sample will be 10 times this. Default is 1000.')
    parser.add_argument('-k'  , '--kernel_type'       , dest='kernel_type'        ,type=str     ,default='Exponential',help='(string) Name of the weighting or kernel function to use. Only option available is [Exponential], but can add others easily.')
    parser.add_argument('-st' , '--sigma_type'        , dest='sigma_type'        ,type=str     ,default='Constant',help='(str) Type of sigma in Eq 3.13 in paper arXiv:1006.3019v2. \
                                                                                                                        Options available [Constant,PDFDependent]. Default is Constant.')
    parser.add_argument('-sb' , '--sigma_bar'         , dest='sigma_bar'         ,type=float   ,default=0.01      ,help='(float) Value of constant sigma_bar (Eq. 3.13 in paper arXiv:1006.3019v2). Default is 0.01.')
    parser.add_argument('-np' , '--npermuations'      , dest='npermuations'      ,type=int     ,default=500       ,help='(int) Number of permutation tests conducted to get test statsitic distribution. Default is 500.')
    parser.add_argument('-ewt', '--eucl_weight_type'  , dest='eucl_weight_type'  ,type=str     ,default='MaxMin'  ,help='(str) Type of weight used for euclidean distance. Options are [MaxMin, RMS, Ones]. Default is MaxMin.')
    parser.add_argument('-imc', '--include_mcterm'    , dest='include_mcterm'    ,type=str2bool,default='False'   ,help='(str) Set to True is you want to include the MC term in the Phi function calculation. Default is False.')
    parser.add_argument('-c'  , '--case'              , dest='case'              ,type=str     ,default='Case1'   ,help='(str) Case study to run. Options are Case[1,2,3]. \
                                                                                                                      Case1: Data and Fit samples are from same PDF. \
                                                                                                                      Case2: Data and Fit samples are from different PDF. However PDFs are only slightly different.\
                                                                                                                      Case3: Data and Fit samples are from different PDF. However PDFs are very different. \
                                                                                                                      Default is Case1')

    args       = parser.parse_args()
    seed       = args.seed
    ndata      = args.ndata
    kernel_type   = args.kernel_type
    sigma_type    = args.sigma_type
    sigma_bar     = args.sigma_bar
    npermuations  = args.npermuations
    eucl_weight_type  = args.eucl_weight_type
    include_mcterm    = args.include_mcterm
    case  = args.case
    print(args)

    main()
