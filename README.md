# Unbinned Goodness of Fit

Implementation of unbinned goodness of fit tests, outlined in this [paper](https://arxiv.org/abs/1006.3019).

Currently there are only two methods implemented:

- Mixed sample test
- Point-to-Point Dissimilarity test (or Energy test). 

The implementation uses numpy package. 

The test conducted is with a 2D Gaussian PDF, where it was checked that the p-value distribution is uniform when parent PDF is equal to test PDF. And for 'Mixed sample test' it was checked that the test statistic for 'Mixed sample test' follows a normal distribution. 

Few points and TODO:

- For the 'Energy test' it is not the most optimal implementation as it takes quite some time for a sample size > 5000. 
- Implement the other methods hilighted in the paper. 

# How to run

```python
python MixedSample.py -h
```

```
Arguments for MixedSample.py

optional arguments:
  -h, --help            show this help message and exit
  -s SEED, --seed SEED  (int) Seed for generation of fake/toy data. Default is
                        1.
  -nd NDATA, --ndata NDATA
                        (int) Sample size for fake/toy data to generate. Test
                        sample will be 10 times this. Default is 1000.
  -nmc NMC, --nMC NMC   (int) Sample size for test sample. Default is 10 times
                        that of default ndata.
  -nk NEAREST_NEIGHBOURS, --nearest_neighbours NEAREST_NEIGHBOURS
                        (int) Number of nearest neighbors to a given point to
                        consider. Default is 10.
  -ewt EUCL_WEIGHT_TYPE, --eucl_weight_type EUCL_WEIGHT_TYPE
                        (str) Type of weight used for euclidean distance.
                        Options are [MaxMin, RMS, Ones]. Default is MaxMin.
  -c CASE, --case CASE  (str) Case study to run. Options are Case[1,2,3].
                        Case1: Data and Fit samples are from same PDF. Case2:
                        Data and Fit samples are from different PDF. However
                        PDFs are only slightly different. Case3: Data and Fit
                        samples are from different PDF. However PDFs are very
                        different. Default is Case1
```

```python
python EnergyTest.py -h
```

```
Arguments for EnergyTest.py

optional arguments:
  -h, --help            show this help message and exit
  -s SEED, --seed SEED  (int) Seed for generation of fake/toy data. Default is
                        1.
  -n NDATA, --ndata NDATA
                        (int) Sample size for fake/toy data to generate. Test
                        sample will be 10 times this. Default is 1000.
  -k KERNEL_TYPE, --kernel_type KERNEL_TYPE
                        (string) Name of the weighting or kernel function to
                        use. Only option available is [Exponential], but can
                        add others easily.
  -st SIGMA_TYPE, --sigma_type SIGMA_TYPE
                        (str) Type of sigma in Eq 3.13 in paper
                        arXiv:1006.3019v2. Options available
                        [Constant,PDFDependent]. Default is Constant.
  -sb SIGMA_BAR, --sigma_bar SIGMA_BAR
                        (float) Value of constant sigma_bar (Eq. 3.13 in paper
                        arXiv:1006.3019v2). Default is 0.01.
  -np NPERMUATIONS, --npermuations NPERMUATIONS
                        (int) Number of permutation tests conducted to get
                        test statsitic distribution. Default is 500.
  -ewt EUCL_WEIGHT_TYPE, --eucl_weight_type EUCL_WEIGHT_TYPE
                        (str) Type of weight used for euclidean distance.
                        Options are [MaxMin, RMS, Ones]. Default is MaxMin.
  -imc INCLUDE_MCTERM, --include_mcterm INCLUDE_MCTERM
                        (str) Set to True is you want to include the MC term
                        in the Phi function calculation. Default is False.
  -c CASE, --case CASE  (str) Case study to run. Options are Case[1,2,3].
                        Case1: Data and Fit samples are from same PDF. Case2:
                        Data and Fit samples are from different PDF. However
                        PDFs are only slightly different. Case3: Data and Fit
                        samples are from different PDF. However PDFs are very
                        different. Default is Case1
```
